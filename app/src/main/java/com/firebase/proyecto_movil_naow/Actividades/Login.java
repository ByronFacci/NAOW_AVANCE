package com.firebase.proyecto_movil_naow.Actividades;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebBackForwardList;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.firebase.proyecto_movil_naow.Bienvendido;
import com.firebase.proyecto_movil_naow.R;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;

public class Login extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener{

    private EditText usuario, contraseña;
    private Button iniciar, registrarse, recuperarclave;
    private ProgressDialog progressDialog;
    private ViewFlipper viewFlipper;
    private FirebaseAuth firebaseAuth;
    public static final int SIGN_IN_CODE = 777;
    private GoogleApiClient googleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        usuario = (EditText)findViewById(R.id.TXTUser);
        contraseña = (EditText)findViewById(R.id.TXTPass);
        iniciar = (Button)findViewById(R.id.BTNIniciarSesion);
        registrarse = (Button)findViewById(R.id.BTNRegistro);
        recuperarclave = (Button)findViewById(R.id.BTNRecuperar);
        iniciar.setOnClickListener(this);
        registrarse.setOnClickListener(this);
        recuperarclave.setOnClickListener(this);
        progressDialog = new ProgressDialog(this);
        firebaseAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, (GoogleApiClient.OnConnectionFailedListener) this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        viewFlipper = (ViewFlipper)findViewById(R.id.ViewFliper);

        int imagenes[] = {R.drawable.energy, R.drawable.alcalina, R.drawable.logo, R.drawable.gas, R.drawable.smart};

        for (int i = 0; i< imagenes.length; i++){
            fliper(imagenes[i]);
        }

    }

    public void fliper(int image){
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);

        viewFlipper.addView(imageView);
        viewFlipper.setFlipInterval(4000);
        viewFlipper.setAutoStart(true);

        viewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        viewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.BTNIniciarSesion:
                if (usuario.getText().toString().isEmpty()) {
                    usuario.setError(getResources().getString(R.string.error));

                } else if (contraseña.getText().toString().isEmpty()) {
                    contraseña.setError(getResources().getString(R.string.error));

                } else {
                    if (usuario.getText().toString().isEmpty()) {
                        usuario.setError(getResources().getString(R.string.error));

                    } else if (contraseña.getText().toString().isEmpty()) {
                        contraseña.setError(getResources().getString(R.string.error));

                    } else {
                        final String correo = usuario.getText().toString().trim();
                        String clave = contraseña.getText().toString().trim();
                        progressDialog.setMessage("ACCEDIENDO..!");
                        progressDialog.show();
                        firebaseAuth.signInWithEmailAndPassword(correo, clave).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    int pos = correo.indexOf("@");
                                    String user = correo.substring(0, pos);
                                    Toast.makeText(Login.this, "BIENVENIDO", Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(Login.this, Bienvendido.class);
                                    intent.putExtra(Bienvendido.user, user);
                                    startActivity(intent);
                                    usuario.setText("");
                                    contraseña.setText("");
                                    usuario.requestFocus();
                                } else {
                                    Toast.makeText(Login.this, "NO EXISTE EL USUARIO..!", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    }

                }
                break;

            case R.id.BTNRegistro:
                if (usuario.getText().toString().isEmpty()) {
                    usuario.setError(getResources().getString(R.string.error));

                } else if (contraseña.getText().toString().isEmpty()) {
                    contraseña.setError(getResources().getString(R.string.error));

                } else {
                    String correo = usuario.getText().toString().trim();
                    String clave = contraseña.getText().toString().trim();
                    progressDialog.setMessage("REGISTRANDO USUARIO..!");
                    progressDialog.show();
                    firebaseAuth.createUserWithEmailAndPassword(correo, clave).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.getException() instanceof FirebaseAuthUserCollisionException) {

                                Toast.makeText(Login.this, "EL USUARIO YA SE ENCUENTRA REGISTRADO..!", Toast.LENGTH_LONG).show();
                                usuario.setText("");
                                contraseña.setText("");
                                progressDialog.hide();
                                usuario.requestFocus();

                            } else if (task.isSuccessful()) {
                                Toast.makeText(Login.this, "USUARIO REGISTRADO", Toast.LENGTH_LONG).show();
                                usuario.setText("");
                                contraseña.setText("");
                                usuario.requestFocus();
                            } else {
                                Toast.makeText(Login.this, "NO SE PUDO REGISTRAR EL USUARIO..!", Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
                break;

            case R.id.BTNRecuperar:
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
               startActivityForResult(intent, SIGN_IN_CODE);

                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SIGN_IN_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            goMainScreen();
        } else {
            Toast.makeText(this, R.string.not_log_in, Toast.LENGTH_SHORT).show();
        }
    }

    private void goMainScreen() {
        Intent intent = new Intent(this, Bienvendido.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        return super.onTouchEvent(event);
    }
}
