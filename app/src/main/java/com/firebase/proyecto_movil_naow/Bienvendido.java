package com.firebase.proyecto_movil_naow;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Bienvendido extends AppCompatActivity {

    public static final String user= "names";
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bienvendido);
        textView = (TextView)findViewById(R.id.TXTUss);
        String usuario = getIntent().getStringExtra("names");
        textView.setText("BIENVENIDO "+ usuario+"..!");
    }
}
